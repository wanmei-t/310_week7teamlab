public class ShiftedTalker extends Talker {
    private int shiftedIndex;
    
    public ShiftedTalker(int shiftedIndex)
    {
        this.shiftedIndex = shiftedIndex;
    }

    public void talk(String message)
    {
        if (message.length()-1<shiftedIndex)
        {
            throw new IllegalArgumentException("The message is too small");
        }
        for (int i = this.shiftedIndex; i < message.length(); i++)
        {
            System.out.print(message.charAt(i));
        }
        for (int i = 0; i < this.shiftedIndex; i++)
        {
            System.out.print(message.charAt(i));
        }
        System.out.println();
    }
}
