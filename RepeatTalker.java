public class RepeatTalker extends Talker{
    private int n;
    
    public int RepeatTalker (int n)
    {
        return this.n =n;
    }

  
    public void talk(String repeatText){
        if (n < 0) 
        {
            throw new IllegalArgumentException("The value of n can not be negative.");
        }
        for (int i = 0; i < n; i++) 
        {
            System.out.println(repeatText);
        }
    
    }

}